<?php

namespace Imawrsham\PayfixPackage;

use Imawrsham\PayfixPackage\Commands\PayfixModelsCommand;
use Illuminate\Support\ServiceProvider;

class PayfixModelsServiceProvider extends ServiceProvider
{
    public function boot()
    {
        if ($this->app->runningInConsole()) {
            $this->publishes([
                __DIR__ . '/../config/payfix-models.php' => config_path('payfix-models.php'),
            ], 'config');

            $this->publishes([
                __DIR__ . '/../resources/views' => base_path('resources/views/vendor/payfix-models'),
            ], 'views');

            $migrationFileName = 'create_payfix_models_table.php';
            if (! $this->migrationFileExists($migrationFileName)) {
                $this->publishes([
                    __DIR__ . "/../database/migrations/{$migrationFileName}.stub" => database_path('migrations/' . date('Y_m_d_His', time()) . '_' . $migrationFileName),
                ], 'migrations');
            }

            $this->commands([
                PayfixModelsCommand::class,
            ]);
        }

        $this->loadViewsFrom(__DIR__ . '/../resources/views', 'payfix-models');
    }

    public function register()
    {
        $this->mergeConfigFrom(__DIR__ . '/../config/payfix-models.php', 'payfix-models');
    }

    public static function migrationFileExists(string $migrationFileName): bool
    {
        $len = strlen($migrationFileName);
        foreach (glob(database_path("migrations/*.php")) as $filename) {
            if ((substr($filename, -$len) === $migrationFileName)) {
                return true;
            }
        }

        return false;
    }
}
