<?php

namespace Imawrsham\PayfixPackage\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Imawrsham\PayfixPackage\Models\Article;
use Imawrsham\PayfixPackage\Models\Discount;
use Imawrsham\PayfixPackage\Models\Mwst;
use Imawrsham\PayfixPackage\Models\Transaction;

class TransactionDetail extends Model
{
    public function transaction()
    {
        return $this->belongsTo(Transaction::class, 'transaction_id', 'id');
    }

    public function article()
    {
        return $this->belongsTo(Article::class, 'article_id', 'id');
    }

    public function freeArticle()
    {
        return $this->article()->where('free', '=', 1)->where('name', '=', 'Trinkgeld');
    }

    public function depositArticle()
    {
        return $this->article()->where('deposit', '=', 1)->where('price', '<', 0);
    }

    public function reportArticle()
    {

        return $this->article()->select('id', 'deposit', 'tip');
    }

    public function discount()
    {
        return $this->belongsTo(Discount::class, 'discount_id', 'id');
    }

    public function mwst()
    {
        return $this->belongsTo(Mwst::class, 'article_mwst_id', 'id');
    }

    public function grouppedTransactionDetails()
    {
        $transactionDetails = $this->hasMany(self::class, 'transaction_id', 'id')
            ->groupBy([
                'article_id',
                'article_price',
                'article_name',
                'article_color',
                'discount_id',
                'discount_percent',
                'article_mwst_id',
                'article_mwst_percent',
                'article_final_price', ])
            ->select(
                [
                    'article_id',
                    'article_price',
                    'article_name',
                    'article_color',
                    'discount_id',
                    'discount_percent',
                    'article_mwst_id',
                    'article_mwst_percent',
                    'article_final_price',
                    DB::raw('COUNT(*) as article_count'),
                    DB::raw('SUM(article_final_price) as articles_final_price'),
                    DB::raw('(SUM(article_price) - SUM(article_final_price)) as articles_discount_amount'), ])
            ->get();

        return $transactionDetails;
    }
}
