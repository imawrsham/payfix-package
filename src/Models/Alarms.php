<?php

namespace Imawrsham\PayfixPackage\Models;

use App\Scopes\Entities\ActiveCompanyScope;
use Illuminate\Database\Eloquent\Model;
use Imawrsham\PayfixPackage\Models\AlarmsLogs;
use Imawrsham\PayfixPackage\Models\AlarmsSender;
use App\Company;

class Alarms extends Model
{
    protected $fillable = [
        'name',
        'company_id',
        'value'];

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    public static function boot()
    {
        parent::boot();
        static::addGlobalScope(new ActiveCompanyScope);
    }

    public function company()
    {
        return $this->hasOne(Company::class, 'id', 'company_id');
    }

    public function alarmsSender()
    {
        return $this->hasMany(AlarmsSender::class, 'alarm_id')->where('sender', '!=', null);
    }

    public function alarmsLogs()
    {
        return $this->hasMany(AlarmsLogs::class, 'alarm_id');
    }

}
