<?php

namespace Imawrsham\PayfixPackage\Models;

use Illuminate\Database\Eloquent\Model;
use Imawrsham\PayfixPackage\Models\Printer;
use Imawrsham\PayfixPackage\Models\RestaurantTableGroup;

class TableGroupPrinter extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'restaurant_table_groups_id',
        'printer_id',
    ];

    // relations

    public function restaurantTableGroup()
    {
        return $this->belongsTo(RestaurantTableGroup::class);
    }

    public function printer()
    {
        return $this->belongsTo(Printer::class);
    }
}
