<?php

namespace Imawrsham\PayfixPackage\Models;

use Illuminate\Database\Eloquent\Collection;
use Imawrsham\PayfixPackage\Models\Merchant;

class ReportingMerchantsWithMwst extends Collection
{
    public static function prepareMerchants(Collection $merchants)
    {
        foreach ($merchants as $merchant) {
            /** @var Merchant $merchant */
            $merchant['deduction_already_paid_mwst'] = $merchant->deductionAlreadyPaidMwst;
            $merchant['rent_percent_mwst'] = $merchant->rentPercentMwst;
            $merchant['fix_rent_mwst'] = $merchant->fixRentMwst;
            $merchant['tent_rent_mwst'] = $merchant->tentRentMwst;
            $merchant['floor_rent_mwst'] = $merchant->floorRentMwst;
            $merchant['electricity_cost_mwst'] = $merchant->electricityCostMwst;
            $merchant['electricity_cost_mwst'] = $merchant->electricityCostMwst;
            $merchant['misc_cost_mwst'] = $merchant->miscCostMwst;
            $merchant['cash_rent_mwst'] = $merchant->cashRentMwst;
            $merchant['adjustment_fees_mwst'] = $merchant->adjustmentFeesMwst;
            $merchant['cash_register_mwst'] = $merchant->cashRegisterMwst;
        }

        return $merchants;
    }
}
