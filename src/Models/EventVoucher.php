<?php

namespace Imawrsham\PayfixPackage\Models;

use Illuminate\Database\Eloquent\Model;
use App\Company;
use App\Event;
use Imawrsham\PayfixPackage\Models\Voucher;

class EventVoucher extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'company_id',
        'event_id',
        'voucher_id',
    ];

    public function companyName()
    {
        return $this->company->name;
    }

    public function company()
    {
        return $this->hasOne(Company::class, 'id', 'company_id');
    }

    public function eventName()
    {
        return $this->event->name;
    }

    public function event()
    {
        return $this->hasOne(Event::class, 'id', 'event_id');
    }

    public function voucherName()
    {
        return $this->voucher->name;
    }

    public function voucher()
    {
        return $this->hasOne(Voucher::class, 'id', 'voucher_id');
    }
}
