<?php

namespace Imawrsham\PayfixPackage\Models;

use Illuminate\Database\Eloquent\Model;
use Imawrsham\PayfixPackage\Models\Device;
use Imawrsham\PayfixPackage\Models\Employee;
use Imawrsham\PayfixPackage\Models\OpenRestaurantTableBasket;
use Imawrsham\PayfixPackage\Models\PointOfSale;
use Imawrsham\PayfixPackage\Models\RestaurantTable;
use Imawrsham\PayfixPackage\Models\RestaurantTableGroup;

class OpenRestaurantTable extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'restaurant_table_id',
        'restaurant_table_group_id',
        'number',
        'table_status',
        'open_date',
        'opener_employee_id',
        'opener_device_id',
        'last_order_date',
        'last_order_employee_id',
        'last_order_device_id',
        'point_of_sale_id',
        'order_sequence',
    ];

    public function restaurantTable()
    {
        return $this->belongsTo(RestaurantTable::class, 'restaurant_table_id', 'id');
    }

    public function pointOfSale()
    {
        return $this->belongsTo(PointOfSale::class, 'point_of_sale_id', 'id');
    }

    public function restaurantTableGroup()
    {
        return $this->belongsTo(RestaurantTableGroup::class, 'restaurant_table_group_id', 'id');
    }

    public function openerEmployee()
    {
        return $this->belongsTo(Employee::class, 'opener_employee_id', 'id');
    }

    public function openerDevice()
    {
        return Device::getDeviceByDeviceIdAndPointOfSaleId($this->opener_device_id, $this->point_of_sale_id);
    }

    public function lastOrderEmployee()
    {
        return $this->belongsTo(Employee::class, 'last_order_employee_id', 'id');
    }

    public function lastOrderDevice()
    {
        return Device::getDeviceByDeviceIdAndPointOfSaleId($this->last_order_device_id, $this->point_of_sale_id);
    }

    public function getTotalAmount()
    {
        return $this->openRestaurantTableBaskets()->sum('article_final_price');
    }

    public function openRestaurantTableBaskets()
    {
        return $this->hasMany(OpenRestaurantTableBasket::class, 'open_restaurant_table_id', 'id');
    }

    public function getOpenRestaurantTableName()
    {
        if ($this->order_sequence == null) {
            return $this->number.' - '.$this->name;
        } else {
            return $this->number.' - '.$this->name.' ('.$this->order_sequence.')';
        }
    }
}
