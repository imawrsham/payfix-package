<?php

namespace Imawrsham\PayfixPackage\Models;

use Illuminate\Database\Eloquent\Collection;
use Imawrsham\PayfixPackage\Models\Transaction;

class ReportingRowData extends Collection
{
    public static function prepareRawData(Collection $transactions)
    {
        foreach ($transactions as $transaction) {
            $trans = Transaction::find($transaction->id);
            /** @var Transaction $transaction */
            $transaction['company'] = $trans->company->name;
            $transaction['event'] = $trans->event->name;
            if ($trans->occasion_id) {
                $transaction['occasion'] = $trans->occasion->name;
                $transaction['occasionGroup'] = $trans->occasion->occasionGroups->name;
            }
            $transaction['point_of_sale'] = $trans->pointOfSale->name;
            $transaction['device'] = $trans->device->name;
            $employee = [];
            array_push($employee,$trans->employee->first_name,$trans->employee->last_name);
            $transaction['employee'] = implode(' ', $employee);
            $transaction['articles'] = $trans->transactionDetailsCustomerReport;
            if ($trans->reportTransactionDetails) {
                foreach ($trans->transactionDetails as $details){
                    $transaction['transactionDetails']['article'] = $details->reportArticle;
                    $transaction['transactionDetails']['article']['articleGroup'] = $details->article->reportArticleGroup;
                }
            }
            $transaction['anzahl'] = count($trans->transactionDetails);
            $transaction['total'] = $trans->transactionSumPrice();
            $transaction['booking_id'] = $trans->booking_id;
            $transaction['Zahlart'] = $trans->pay_type;
            $transaction['Konto'] = $trans->account->name;
            $transaction['Teiltyp'] = $trans->partial_type;
            $transaction['Zahlungszeit'] = date('d.m.Y-H:i:s', strtotime($trans->pay_time));
            if ($trans->cardPaymentDetails){
                $transaction['card_payment_detail'] = $trans->reportCardPaymentDetails;
            }
        }

        return $transactions;
    }
}
