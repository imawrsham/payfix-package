<?php

namespace Imawrsham\PayfixPackage\Models;

use Illuminate\Database\Eloquent\Model;

class AlarmsSender extends Model
{
    protected $fillable = [
        'alarm_id',
        'sender'];

}
