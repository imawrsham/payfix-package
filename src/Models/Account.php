<?php

namespace Imawrsham\PayfixPackage\Models;

use App\Scopes\Entities\ActiveCompanyScope;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Company;
use Imawrsham\PayfixPackage\Models\MainModel;
use Imawrsham\PayfixPackage\Models\PointOfSale;

class Account extends MainModel
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'company_id',
        'name',
        'active',
    ];

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    public static function boot()
    {
        parent::boot();
        static::addGlobalScope(new ActiveCompanyScope);
    }

    public function companyName()
    {
        return $this->company->name;
    }

    public function company()
    {
        return $this->hasOne(Company::class, 'id', 'company_id');
    }

    public function pointOfSales()
    {
        return $this->belongsToMany(PointOfSale::class, 'point_of_sale_accounts', 'account_id', 'point_of_sale_id');
    }

    public function pointOfSaleIds()
    {
        return $this->pointOfSales->pluck('id')->toArray();
    }
}
