<?php

namespace Imawrsham\PayfixPackage\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Imawrsham\PayfixPackage\Models\GoodsManagerTransactions;

class GoodsManagerTransactionDetails extends Model
{
    use HasFactory;
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'goods_manager_id',
        'point_of_sale_id',
        'article_id',
        'quantity',
    ];

    public function transaction(){
        return $this->belongsTo(GoodsManagerTransactions::class, 'goods_manager_id', 'id');
    }
}
