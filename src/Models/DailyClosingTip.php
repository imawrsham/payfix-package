<?php

namespace Imawrsham\PayfixPackage\Models;

use Illuminate\Database\Eloquent\Model;
use Imawrsham\PayfixPackage\Models\DailyClosing;
use Imawrsham\PayfixPackage\Models\Employee;

class DailyClosingTip extends Model
{
    public function DailyClosing()
    {
        return $this->belongsTo(DailyClosing::class, 'daily_closing_id', 'id');
    }

    public function employee()
    {
        return $this->hasOne(Employee::class, 'id', 'employee_id');
    }

    public function employeeWithTrashed()
    {
        return $this->hasOne(Employee::class, 'id', 'employee_id')->withTrashed();
    }
}
