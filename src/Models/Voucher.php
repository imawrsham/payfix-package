<?php

namespace Imawrsham\PayfixPackage\Models;

use App\Scopes\Entities\VoucherScope;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Company;
use Imawrsham\PayfixPackage\Models\EventVoucher;
use Imawrsham\PayfixPackage\Models\MainModel;
use Imawrsham\PayfixPackage\Models\PointOfSale;

class Voucher extends MainModel
{
    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'company_id',
        'name',
        'show_in_e_order',
    ];

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    public static function boot()
    {
        parent::boot();
        static::addGlobalScope(new VoucherScope);
    }

    public function companyName()
    {
        return $this->company->name;
    }

    public function company()
    {
        return $this->hasOne(Company::class, 'id', 'company_id');
    }

    public function eventVouchers()
    {
        return $this->hasMany(EventVoucher::class, 'voucher_id', 'id')->get();
    }

    public function eventNames()
    {
        return implode(', ', $this->eventVouchers()->pluck('event.name')->all());
    }

    public function pointOfSales()
    {
        return $this->belongsToMany(PointOfSale::class, 'point_of_sale_vouchers', 'voucher_id', 'point_of_sale_id');
    }

    public function pointOfSaleIds()
    {
        return $this->pointOfSales->pluck('id')->toArray();
    }

    public function validFrom()
    {
        return date('d.m.Y H:i', strtotime($this->valid_from));
    }

    public function validTo()
    {
        return date('d.m.Y H:i', strtotime($this->valid_to));
    }
}
