<?php

namespace Imawrsham\PayfixPackage\Models;

use Illuminate\Database\Eloquent\Model;
use Imawrsham\PayfixPackage\Models\Article;
use Imawrsham\PayfixPackage\Models\Discount;
use Imawrsham\PayfixPackage\Models\Mwst;
use Imawrsham\PayfixPackage\Models\OpenRestaurantTable;
use Imawrsham\PayfixPackage\Models\PointOfSale;
use Imawrsham\PayfixPackage\Models\RestaurantTable;

class OpenRestaurantTableBasket extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'article_id',
        'article_is_deposit',
        'article_price',
        'article_name',
        'article_sorting',
        'article_color',
        'article_deposit_id',
        'article_is_discountable',
        'article_mwst_id',
        'article_mwst_percent',
        'discount_id',
        'discount_percent',
        'is_discount_from_basket',
        'article_final_price',
        'article_name_in_checkout',
        'article_is_free',
        'point_of_sale_id',
        'restaurant_table_id',
        'is_new',
        'insert_date',
        'order_sequence',
        'open_restaurant_table_id',
        'basket_id',
        'parent_order_sequence',
        'article_package_type',
    ];

    public function article()
    {
        return $this->belongsTo(Article::class, 'article_id', 'id');
    }

    public function articleDeposit()
    {
        return $this->belongsTo(Article::class, 'article_deposit_id', 'id');
    }

    public function articleMwst()
    {
        return $this->belongsTo(Mwst::class, 'article_mwst_id', 'id');
    }

    public function discount()
    {
        return $this->belongsTo(Discount::class, 'discount_id', 'id');
    }

    public function pointOfSales()
    {
        return $this->belongsTo(PointOfSale::class, 'point_of_sale_id', 'id');
    }

    public function restaurantTable()
    {
        return $this->belongsTo(RestaurantTable::class, 'restaurant_table_id', 'id');
    }

    public function openRestaurantTable()
    {
        return $this->belongsTo(OpenRestaurantTable::class, 'open_restaurant_table_id', 'id');
    }
}
