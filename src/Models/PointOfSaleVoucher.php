<?php

namespace Imawrsham\PayfixPackage\Models;

use Illuminate\Database\Eloquent\Model;
use Imawrsham\PayfixPackage\Models\PointOfSale;
use Imawrsham\PayfixPackage\Models\Voucher;

class PointOfSaleVoucher extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'point_of_sale_id',
        'voucher_id',
    ];

    protected $primaryKey = ['point_of_sale_id', 'voucher_id'];
    public $incrementing = false;

    // relations

    public function qrCodeCategory()
    {
        return $this->hasOne(PointOfSale::class, 'id', 'point_of_sale_id');
    }

    public function voucher()
    {
        return $this->hasOne(Voucher::class, 'id', 'voucher_id');
    }

    /**
     * Set the keys for a save update query.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    protected function setKeysForSaveQuery($query)
    {
        $keys = $this->getKeyName();
        if (! is_array($keys)) {
            return parent::setKeysForSaveQuery($query);
        }

        foreach ($keys as $keyName) {
            $query->where($keyName, '=', $this->getKeyForSaveQuery($keyName));
        }

        return $query;
    }

    /**
     * Get the primary key value for a save query.
     *
     * @param mixed $keyName
     * @return mixed
     */
    protected function getKeyForSaveQuery($keyName = null)
    {
        if (is_null($keyName)) {
            $keyName = $this->getKeyName();
        }

        if (isset($this->original[$keyName])) {
            return $this->original[$keyName];
        }
    }
}
