<?php

namespace Imawrsham\PayfixPackage\Models;

use Imawrsham\PayfixPackage\Models\Article;
use App\Company;
use Imawrsham\PayfixPackage\Models\MainModel;

class Mwst extends MainModel
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'mwst',
    ];

    // functions

    public function decimal()
    {
        if ($this->mwst > 0) {
            return $this->mwst / 100;
        } else {
            return $this->mwst;
        }
    }

    public function amount()
    {
        $formated_mwst = number_format($this->mwst, 2, '.', "'");

        if (substr($formated_mwst, -1) == '0') {
            $formated_mwst = rtrim($formated_mwst, '0');
        }

        return implode('', [$formated_mwst, ' %']);
    }

    public function canBeEdited()
    {
        return count($this->articles) === 0;
    }

    public function companies()
    {
        return $this->belongsToMany(Company::class, 'mwst_companies', 'mwst_id', 'company_id');
    }

    public function companiesIds()
    {
        return $this->companies()->pluck('companies.id')->toArray();
    }

    public function articles()
    {
        return $this->hasMany(Article::class, 'mwst_id');
    }
}
