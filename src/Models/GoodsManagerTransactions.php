<?php

namespace Imawrsham\PayfixPackage\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GoodsManagerTransactions extends Model
{
    use HasFactory;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'point_of_sale_id',
        'article_id',
        'type',
        'occasion_id'
    ];
}
