<?php

namespace Imawrsham\PayfixPackage\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Imawrsham\PayfixPackage\Models\ArticlePackage;
use Imawrsham\PayfixPackage\Models\MainModel;
use Imawrsham\PayfixPackage\Models\StepItem;

class Step extends MainModel
{
    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'article_package_id',
        'name',
        'priority',
        'minimum_items',
        'maximum_items',
    ];

    public function articlePackage()
    {
        return $this->belongsTo(ArticlePackage::class, 'article_package_id', 'id');
    }

    public function stepItems()
    {
        return $this->hasMany(StepItem::class, 'step_id', 'id')->orderBy('priority', 'ASC');
    }
}
