<?php

namespace Imawrsham\PayfixPackage\Models;

use App\Scopes\Entities\ActiveCompanyScope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Imawrsham\PayfixPackage\Models\Account;
use Imawrsham\PayfixPackage\Models\CardPaymentDetail;
use App\Company;
use Imawrsham\PayfixPackage\Models\Credit;
use Imawrsham\PayfixPackage\Models\Customer;
use Imawrsham\PayfixPackage\Models\Device;
use Imawrsham\PayfixPackage\Models\Employee;
use App\Event;
use Imawrsham\PayfixPackage\Models\Occasions;
use Imawrsham\PayfixPackage\Models\PointOfSale;
use Imawrsham\PayfixPackage\Models\QrCode;
use Imawrsham\PayfixPackage\Models\QrCodeCredit;
use Imawrsham\PayfixPackage\Models\QrCodeVoucher;
use Imawrsham\PayfixPackage\Models\RestaurantTable;
use Imawrsham\PayfixPackage\Models\TransactionDetail;
use Imawrsham\PayfixPackage\Models\Voucher;

class Transaction extends Model
{
    protected $fillable = [
        'credit_id',
        'point_of_sale_id',
        'pay_time',
        'is_cancelled',
        'amount',
        'booking_id',
        'company_id',
        'employee_id',
        'account_id',
        'device_id',
        'event_id',
        'occasion_id',
        'qr_code_id',
        'pay_type',
        'partial_type'];

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    public static function boot()
    {
        parent::boot();
        static::addGlobalScope(new ActiveCompanyScope);
    }

    const TRANSACTION_TYPE_QR_CODE_CHARGE = 'QRCodeCharge';
    const TRANSACTION_TYPE_QR_CODE_DISCHARGE = 'QRCodeDischarge';
    const TRANSACTION_TYPE_PAY = 'Pay';
    const TRANSACTION_TYPE_CANCELLATION = 'Cancelation';
    const TRANSACTION_TYPE_QR_CODE_CHARGE_CANCELLATION = 'QRCodeChargeCancellation';
    const TRANSACTION_TYPE_QR_CODE_DISCHARGE_CANCELLATION = 'QRCodeDischargeCancellation';

    const TRANSACTION_PARTIAL_TYPE_HAS_PARTIAL = 'HasPartialTransaction';
    const TRANSACTION_PARTIAL_TYPE_IS_PARTIAL = 'IsPartialTransaction';
    const TRANSACTION_PARTIAL_TYPE_NOT_PARTIAL = 'NotPartial';

    public function company()
    {
        return $this->hasOne(Company::class, 'id', 'company_id');
    }

    public function companyWithTrashed()
    {
        return $this->hasOne(Company::class, 'id', 'company_id')->withTrashed();
    }

    public function event()
    {
        return $this->hasOne(Event::class, 'id', 'event_id');
    }

    public function eventWithTrashed()
    {
        return $this->hasOne(Event::class, 'id', 'event_id')->withTrashed();
    }

    public function qrCode()
    {
        return $this->hasOne(QrCode::class, 'id', 'qr_code_id');
    }

    public function voucher()
    {
        return $this->hasOne(Voucher::class, 'id', 'voucher_id');
    }

    public function customer()
    {
        return $this->hasOne(Customer::class, 'id', 'customer_id');
    }

    public function customerWithTrashed()
    {
        return $this->hasOne(Customer::class, 'id', 'customer_id')->withTrashed();
    }

    public function employee()
    {
        return $this->hasOne(Employee::class, 'id', 'employee_id');
    }

    public function employeeWithTrashed()
    {
        return $this->hasOne(Employee::class, 'id', 'employee_id')->withTrashed();
    }

    public function credit()
    {
        return $this->hasOne(Credit::class, 'id', 'credit_id');
    }

    public function account()
    {
        return $this->hasOne(Account::class, 'id', 'account_id');
    }

    public function accountWithTrashed()
    {
        return $this->hasOne(Account::class, 'id', 'account_id')->withTrashed();
    }

    public function device()
    {
        return $this->hasOne(Device::class, 'id', 'device_id');
    }

    public function deviceWithTrashed()
    {
        return $this->hasOne(Device::class, 'id', 'device_id')->withTrashed();
    }

    public function pointOfSale()
    {
        return $this->hasOne(PointOfSale::class, 'id', 'point_of_sale_id');
    }

    public function pointOfSaleWithTrashed()
    {
        return $this->hasOne(PointOfSale::class, 'id', 'point_of_sale_id')->withTrashed();
    }

    public function occasion()
    {
        return $this->hasOne(Occasions::class, 'id', 'occasion_id');
    }

    public function occasionWithTrashed()
    {
        return $this->hasOne(Occasions::class, 'id', 'occasions_id')->withTrashed();
    }

    public function transactionDetails()
    {
        return $this->hasMany(TransactionDetail::class, 'transaction_id', 'id');
    }

    public function transactionDetailsCustomerReport()
    {
        return $this->hasMany(TransactionDetail::class, 'transaction_id', 'id')
            ->leftJoin('articles', 'articles.id', '=', 'transaction_details.article_id')
            ->leftJoin('article_groups', 'article_groups.id', '=', 'articles.article_group_id')
            ->select(
                    'articles.name as name',
                    'article_groups.name as articleGroupName',
                    'transaction_details.article_mwst_percent as vat',
                    'transaction_details.article_final_price as price');
    }

    public function transactionSumFinal()
    {
        return $this->hasMany(TransactionDetail::class, 'transaction_id', 'id')->sum('article_final_price');
    }

    public function transactionSumPrice()
    {
        return $this->hasMany(TransactionDetail::class, 'transaction_id', 'id')->sum('article_price');
    }

    public function relatedTransactions()
    {
        return self::where('booking_id', $this->booking_id)->where('id', '!=', $this->id)->orderBy('id', 'DESC')->get();
    }

    public function grouppedTransactionDetails()
    {
        $transactionDetails = $this->hasMany(TransactionDetail::class, 'transaction_id', 'id')
            ->groupBy([
                'article_id',
                'article_price',
                'article_name',
                'article_color',
                'discount_id',
                'discount_percent',
                'article_mwst_id',
                'article_mwst_percent',
                'article_final_price', ])
            ->select(
                [
                    'article_id',
                    'article_price',
                    'article_name',
                    'article_color',
                    'discount_id',
                    'discount_percent',
                    'article_mwst_id',
                    'article_mwst_percent',
                    'article_final_price',
                    DB::raw('COUNT(*) as article_count'),
                    DB::raw('SUM(article_final_price) as articles_final_price'),
                    DB::raw('(SUM(article_price) - SUM(article_final_price)) as articles_discount_amount'), ])
            ->get();

        return $transactionDetails;
    }

    public function transactionDetailsWithArticle()
    {
        return $this->hasMany(TransactionDetail::class, 'transaction_id', 'id')->limit(1000)->with('article')->with(['article.mwst', 'article.articleGroup']);
    }

    public function cardPaymentDetail()
    {
        return $this->hasOne(CardPaymentDetail::class, 'transaction_id', 'id');
    }

    public function reportCardPaymentDetails()
    {
        return $this->cardPaymentDetail()->select(
            'account_number',
            'trans_ref',
            'acq_trans_ref',
            'six_trx_ref_num',
            'trm_trans_ref',
            'auth_code',
            'card_number',
            'time_stamp',
            'aid',
            'tender_name',
            'trans_seq',
        );
    }

    public function qrCodeVouchers()
    {
        return $this->hasMany(QrCodeVoucher::class, 'transaction_id', 'id');
    }

    public function qrCodeCredits()
    {
        return $this->hasMany(QrCodeCredit::class, 'transaction_id', 'id');
    }

    public static function getTransactionsByBookingId($bookingId)
    {
        return self::where('booking_id', $bookingId)->get();
    }

    public function cardPaymentDetails()
    {
        return $this->hasMany(CardPaymentDetail::class, 'transaction_id', 'id');
    }

    public function getCancellation($transaction)
    {
        $query = self::Where([
            ['booking_id', '=', $transaction->booking_id],
            ['partial_type', '=', $transaction->partial_type],
        ]);
        if ($transaction->pay_type == 'Pay') {
            $data = $query->where('pay_type', 'Cancelation')->first();
        } elseif ($transaction->pay_type == 'QRCodeCharge') {
            $data = $query->where('pay_type', 'QRCodeChargeCancellation')->first();
        } elseif ($transaction->pay_type == 'QRCodeDischarge') {
            $data = $query->where('pay_type', 'QRCodeDischargeCancellation')->first();
        } else {
            $data = null;
        }

        if ($data != null) {
            return true;
        } else {
            return false;
        }
    }

    public function restaurantTable()
    {
        return $this->hasOne(RestaurantTable::class, 'id', 'restaurant_table_id');
    }

    public function restaurantTableWithTrashed()
    {
        return $this->hasOne(RestaurantTable::class, 'id', 'restaurant_table_id')->withTrashed();
    }
}
