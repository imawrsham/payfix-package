<?php

namespace Imawrsham\PayfixPackage\Models;

use App\Events\ModelChange;
use App\Scopes\Entities\ActiveCompanyScope;
use Fico7489\Laravel\Pivot\Traits\PivotEventTrait;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;
use Imawrsham\PayfixPackage\Models\Article;
use Imawrsham\PayfixPackage\Models\ArticlePackage;
use Imawrsham\PayfixPackage\Models\Assortment;
use App\Company;
use Imawrsham\PayfixPackage\Models\MainModel;
use Imawrsham\PayfixPackage\Models\Mwst;
use Imawrsham\PayfixPackage\Models\PointOfSale;
use Imawrsham\PayfixPackage\Models\RestaurantTableGroup;
use Imawrsham\PayfixPackage\Models\StandType;
use Imawrsham\PayfixPackage\Models\Step;

class Merchant extends Authenticatable
{
    use Notifiable, SoftDeletes;
    use PivotEventTrait;

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->dispatchesEvents['created'] = ModelChange::class;
        $this->dispatchesEvents['updated'] = ModelChange::class;
        $this->dispatchesEvents['deleted'] = ModelChange::class;
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'company',
        'email',
        'phone_number',
        'account_owner',
        'iban',
        'bic',
        'bank_name',
        'street',
        'plz',
        'city',
        'country',
        'rent_percent',
        'fix_rent',
        'tent_rent',
        'floor_rent',
        'electricity_cost',
        'misc_cost',
        'cash_rent',
        'adjustment_fees',
        'cash_register',
        'internal_external',
        'stand_type_id',
        'payout',
        'settlement_per_point_of_sale',
        'deduction_already_paid',
        'deduction_already_paid_mwst',
        'rent_percent_mwst',
        'fix_rent_mwst',
        'tent_rent_mwst',
        'floor_rent_mwst',
        'electricity_cost_mwst',
        'misc_cost_mwst',
        'cash_rent_mwst',
        'adjustment_fees_mwst',
        'cash_register_mwst',
        'vat_number',
        'receipt_end_text',
    ];

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    public static function boot()
    {
        parent::boot();
        static::addGlobalScope(new ActiveCompanyScope);

        static::pivotAttached(function ($model, $relationName, $pivotIds, $pivotIdsAttributes) {
            MainModel::callModelChangeEvent('insert', $model, $relationName, $pivotIds);
        });

        static::pivotDetached(function ($model, $relationName, $pivotIds) {
            MainModel::callModelChangeEvent('delete', $model, $relationName, $pivotIds);
        });
    }

    public function canBeDeleted()
    {
        return boolval(count($this->pointOfSales) === 0);
    }

    public function pointOfSales()
    {
        return $this->hasMany(PointOfSale::class, 'merchant_id', 'id');
    }

    public function assortments()
    {
        return $this->hasMany(Assortment::class, 'merchant_id', 'id');
    }

    public function articlePackages()
    {
        return $this->hasMany(ArticlePackage::class, 'merchant_id', 'id');
    }

    public function company()
    {
        return $this->hasOne(Company::class, 'id', 'company_id');
    }

    public function standTypeName()
    {
        return $this->standType ? $this->standType->name : '-';
    }

    public function standType()
    {
        return $this->hasOne(StandType::class, 'id', 'stand_type_id');
    }

    public function deductionAlreadyPaidMwst()
    {
        return $this->belongsTo(Mwst::class, 'deduction_already_paid_mwst', 'id');
    }

    public function rentPercentMwst()
    {
        return $this->belongsTo(Mwst::class, 'rent_percent_mwst', 'id');
    }

    public function fixRentMwst()
    {
        return $this->belongsTo(Mwst::class, 'fix_rent_mwst', 'id');
    }

    public function tentRentMwst()
    {
        return $this->belongsTo(Mwst::class, 'tent_rent_mwst', 'id');
    }

    public function floorRentMwst()
    {
        return $this->belongsTo(Mwst::class, 'floor_rent_mwst', 'id');
    }

    public function steps()
    {
        $articlePackage = $this->hasMany(ArticlePackage::class, 'merchant_id', 'id')->pluck('id')->toArray();

        return Step::whereIn('article_package_id', $articlePackage);
    }

    public function tableGroupPrinters()
    {
        $restaurantTableGroup = $this->hasMany(RestaurantTableGroup::class, 'merchant_id', 'id')->pluck('id')->toArray();

        return DB::table('table_group_printers')->whereIn('restaurant_table_group_id', $restaurantTableGroup)->get();
    }

    public function floorMentMwst()
    {
        return $this->belongsTo(Mwst::class, 'floor_rent_mwst', 'id');
    }

    public function electricityCostMwst()
    {
        return $this->belongsTo(Mwst::class, 'electricity_cost_mwst', 'id');
    }

    public function miscCostMwst()
    {
        return $this->belongsTo(Mwst::class, 'misc_cost_mwst', 'id');
    }

    public function cashRentMwst()
    {
        return $this->belongsTo(Mwst::class, 'cash_rent_mwst', 'id');
    }

    public function cashRegisterMwst()
    {
        return $this->belongsTo(Mwst::class, 'cash_register_mwst', 'id');
    }

    public function articleCount()
    {
        $merchantId = $this->id;

        return Article::whereHas('articleGroup', function ($query) use ($merchantId) {
            $query->where('merchant_id', $merchantId);
        })->count();
    }
}
