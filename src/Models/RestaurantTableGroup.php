<?php

namespace Imawrsham\PayfixPackage\Models;

use App\Events\ModelChange;
use App\Scopes\Entities\RestaurantTableGroupScope;
use Illuminate\Database\Eloquent\SoftDeletes;
use Imawrsham\PayfixPackage\Models\MainModel;
use Imawrsham\PayfixPackage\Models\Merchant;
use Imawrsham\PayfixPackage\Models\Printer;
use Imawrsham\PayfixPackage\Models\RestaurantGroupsAllotment;
use Imawrsham\PayfixPackage\Models\RestaurantTable;

class RestaurantTableGroup extends MainModel
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'merchant_id',
        'has_integrated_printer',
        'name',
    ];

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    public static function boot()
    {
        parent::boot();
        static::addGlobalScope(new RestaurantTableGroupScope);
    }

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->dispatchesEvents['updating'] = ModelChange::class;
    }

    public function merchant()
    {
        return $this->hasOne(Merchant::class, 'id', 'merchant_id');
    }

    public function restaurantTable()
    {
        return $this->hasMany(RestaurantTable::class, 'restaurant_table_group_id', 'id');
    }

    public function printers()
    {
        return $this->belongsToMany(Printer::class, 'table_group_printers', 'restaurant_table_group_id', 'printer_id');
    }

    public function printerIds()
    {
        return $this->printers()->pluck('printers.id')->toArray();
    }

    // functions

    public function canBeDeleted()
    {
        return boolval(count($this->restaurantTable) === 0);
    }

    public function hasRestaurantTableGroupAllotment()
    {
        return $this->restaurantTableGroupAllotments()->count() > 0;
    }

    public function restaurantTableGroupAllotments()
    {
        return $this->hasMany(RestaurantGroupsAllotment::class, 'restaurant_table_groups_id', 'id');
    }
}
