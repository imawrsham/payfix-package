<?php

namespace Imawrsham\PayfixPackage\Models;

use App\Scopes\Entities\OccasionScope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Imawrsham\PayfixPackage\Models\OccasionGroups;

class Occasions extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'start_at',
        'end_at',
        'occasion_groups_id',
    ];

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    public static function boot()
    {
        parent::boot();
        static::addGlobalScope(new OccasionScope);
    }

    public function start()
    {
        return date('d.m.Y-H:i:s', strtotime($this->start_at));
    }

    public function end()
    {
        return $this->end_at ? date('d.m.Y-H:i:s', strtotime($this->end_at)) : '-';
    }

    public function range()
    {
        return $this->start().(! is_null($this->end_at) ? ' - '.$this->end() : '');
    }

    public function occasionGroups()
    {
        return $this->hasOne(OccasionGroups::class, 'id', 'occasion_groups_id');
    }

}
