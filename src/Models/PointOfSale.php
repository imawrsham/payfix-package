<?php

namespace Imawrsham\PayfixPackage\Models;

use App\Scopes\Entities\PointOfSaleScope;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use Imawrsham\PayfixPackage\Models\Account;
use Imawrsham\PayfixPackage\Models\Article;
use Imawrsham\PayfixPackage\Models\ArticleAllotment;
use Imawrsham\PayfixPackage\Models\ArticleGroup;
use Imawrsham\PayfixPackage\Models\ArticlePackage;
use Imawrsham\PayfixPackage\Models\Assortment;
use App\Company;
use Imawrsham\PayfixPackage\Models\Device;
use Imawrsham\PayfixPackage\Models\Employee;
use Imawrsham\PayfixPackage\Models\MainModel;
use Imawrsham\PayfixPackage\Models\Merchant;
use Imawrsham\PayfixPackage\Models\OccasionGroups;
use Imawrsham\PayfixPackage\Models\Printer;
use Imawrsham\PayfixPackage\Models\PrinterAllotment;
use Imawrsham\PayfixPackage\Models\RestaurantTable;
use Imawrsham\PayfixPackage\Models\RestaurantTableGroup;
use Imawrsham\PayfixPackage\Models\StandType;
use Imawrsham\PayfixPackage\Models\StepItem;
use Imawrsham\PayfixPackage\Models\Voucher;

class PointOfSale extends MainModel
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'merchant_id',
        'name',
        'terminal_integrated',
        'reporting_id',
        'pin',
        'reset_number',
        'location_id',
        'word_receipt_food_type',
        'word_receipt_place',
        'customer_receipt',
        'receipt_printer_id',
        'has_integrated_receipt_printer',
        'has_integrated_printer',
        'cut_work_receipt',
        'receipt_confirm',
        'normal_basket_work_receipt',
        'allow_normal_basket',
        'show_images_in_android'
    ];

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    public static function boot()
    {
        parent::boot();
        static::addGlobalScope(new PointOfSaleScope);
    }

    // functions

    public function merchantName()
    {
        return $this->merchant ? $this->merchant->name : '';
    }

    // relations

    public function articleAllotments()
    {
        return $this->hasMany(ArticleAllotment::class, 'point_of_sale_id', 'id');
    }

    public function merchant()
    {
        return $this->hasOne(Merchant::class, 'id', 'merchant_id');
    }

    public function printers()
    {
        return $this->belongsToMany(Printer::class, 'printer_allotments', 'point_of_sale_id', 'printer_id');
    }

    public function printersNames()
    {
        return implode(', ', $this->printers()->pluck('printers.name')->toArray());
    }

    public function printerIds()
    {
        return $this->printers()->pluck('printers.id')->toArray();
    }

    public function merchantWithTrashed()
    {
        return $this->hasOne(Merchant::class, 'id', 'merchant_id')->withTrashed();
    }

    public function accounts()
    {
        return $this->belongsToMany(Account::class, 'point_of_sale_accounts', 'point_of_sale_id', 'account_id');
    }

    public function locations()
    {
        return $this->location ? $this->location->name : '-';
    }

    public function accountNames()
    {
        return implode(', ', $this->accounts()->pluck('accounts.name')->toArray());
    }

    public function accountIds()
    {
        return $this->accounts()->pluck('accounts.id')->toArray();
    }

    public function vouchers()
    {
        return $this->belongsToMany(Voucher::class, 'point_of_sale_vouchers', 'point_of_sale_id', 'voucher_id');
    }

    public function assortments()
    {
        return $this->belongsToMany(Assortment::class, 'point_of_sale_assortments', 'point_of_sale_id', 'assortment_id');
    }

    public function voucherNames()
    {
        return implode(', ', $this->vouchers()->pluck('vouchers.name')->toArray());
    }

    public function voucherIds()
    {
        return $this->vouchers()->pluck('vouchers.id')->toArray();
    }

    public function assortmentIds()
    {
        return $this->assortments()->pluck('assortments.id')->toArray();
    }

    public function articles()
    {
        return $this->belongsToMany(Article::class, 'article_allotments', 'point_of_sale_id', 'article_id');
    }

    public function articlePackages()
    {
        $articleIds = ArticleAllotment::where('point_of_sale_id', $this->id)->select('article_id')->distinct()->pluck('article_id');
        $articlePackageIds = Article::whereNotNull('article_package_id')->whereIn('id', $articleIds)->select('article_package_id')->distinct()->pluck('article_package_id');

        return ArticlePackage::whereIn('id', $articlePackageIds)
            ->with('articles', 'steps', 'steps.stepItems')
            ->get();
    }

    public function articlesByMerchant()
    {
        return $this->belongsToMany(Article::class, 'article_allotments', 'point_of_sale_id', 'article_id')->wherePivot('merchant_id', '=', $this->merchant_id)
            ->withPivot('point_of_sale_id', 'article_id', 'favourite');
    }

    public function articlePrinters()
    {
        $articleIds = $this->belongsToMany(Article::class, 'article_allotments', 'point_of_sale_id', 'article_id')->pluck('articles.id')->toArray();

        return DB::table('article_printers')->whereIn('article_id', $articleIds)->get();
    }

    public function stepItems()
    {
        $articleIds = $this->belongsToMany(Article::class, 'article_allotments', 'point_of_sale_id', 'article_id')->pluck('articles.id')->toArray();

        return StepItem::whereIn('article_id', $articleIds);
    }

    public function articleAssortments()
    {
        $articleIds = $this->belongsToMany(Article::class, 'article_allotments', 'point_of_sale_id', 'article_id')->pluck('articles.id')->toArray();

        return DB::table('article_assortments')->whereIn('article_id', $articleIds);
    }

    public function steps()
    {
        return $this->belongsToMany(Printer::class, 'printer_allotments', 'point_of_sale_id', 'printer_id');
    }

    public function restaurantTableGroups()
    {
        return $this->belongsToMany(RestaurantTableGroup::class, 'restaurant_groups_allotments', 'point_of_sale_id', 'restaurant_table_groups_id');
    }

    public function restaurantTableGroupNames()
    {
        return implode(', ', $this->restaurantTableGroups()->pluck('restaurant_table_groups.name')->toArray());
    }

    public function restaurantTableGroupId()
    {
        return $this->restaurantTableGroups()->pluck('restaurant_table_groups.id')->toArray();
    }

    public function restaurantTables()
    {
        $restaurantTableGroupsIds = $this->restaurantTableGroups()->pluck('restaurant_table_groups.id')->toArray();

        return RestaurantTable::whereIn('restaurant_table_group_id', $restaurantTableGroupsIds);
    }

    public function standType()
    {
        return $this->hasOne(StandType::class, 'id', 'stand_type_id');
    }

    public function articleGroups()
    {
        $articleGroupIds = $this->articles()->pluck('articles.article_group_id')->toArray();

        return ArticleGroup::whereIn('id', $articleGroupIds);
    }

    public function employees()
    {
        return $this->hasMany(Employee::class, 'point_of_sale_id');
    }

    public function devices()
    {
        return $this->hasMany(Device::class, 'point_of_sale_id');
    }

    public function printerAllotments()
    {
        return $this->hasMany(PrinterAllotment::class, 'point_of_sale_id');
    }

    public function occasionsGroup()
    {
        return $this->hasOne(OccasionGroups::class, 'id', 'occasion_groups_id');
    }

    public function occasionsGroupNames()
    {
        return implode(', ', $this->occasionsGroup()->pluck('occasion_groups.name')->toArray());
    }

    /**
     * @param array $articleAllotments
     * @param Article $article
     * @return bool
     */
    public function isAllotmentExists($articleAllotments, Article $article)
    {
        if ($articleAllotments) {
            foreach ($articleAllotments as $allotment) {
                if ($allotment->point_of_sale_id === $this->id and
                    $allotment->article_id === $article->id and
                    $allotment->merchant_id === $this->merchant_id
                ) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * @param Article $printer
     * @return bool
     */
    public function isPrinterAllotmentExists(Printer $printer)
    {
        return $this->printerAllotments()
            ->where('printer_id', $printer->id)
            ->where('merchant_id', $this->merchant_id)
            ->exists();
    }

    /**
     * @param array $restaurantTableAllotments
     * @param Article $article
     * @return bool
     */
    public function isAllotmentRestaurantTableExists($restaurantTableAllotments, RestaurantTableGroup $restaurantTableGroup)
    {
        if ($restaurantTableAllotments) {
            foreach ($restaurantTableAllotments as $allotment) {
                if ($allotment->point_of_sale_id === $this->id and
                    $allotment->restaurant_table_groups_id === $restaurantTableGroup->id
                ) {
                    return true;
                }
            }
        }

        return false;
    }

    public function getAllotmentDescription($articleAllotments, Article $article)
    {
        if ($articleAllotments) {
            foreach ($articleAllotments as $allotment) {
                if ($allotment->point_of_sale_id === $this->id and
                    $allotment->article_id === $article->id and
                    $allotment->merchant_id === $this->merchant_id
                ) {
                    return $allotment->description;
                }
            }
        }

        return '';
    }

    /**
     * @param $articleAllotments
     * @param Article $article
     * @return string
     */
    public function getAllotmentExists($articleAllotments, Article $article)
    {
        if ($articleAllotments) {
            foreach ($articleAllotments as $allotment) {
                if ($allotment->point_of_sale_id === $this->id and
                    $allotment->article_id === $article->id and
                    $allotment->merchant_id === $this->merchant_id
                ) {
                    return $allotment->merchant_id . ',' . $allotment->point_of_sale_id . ',' . $allotment->article_id;
                }
            }
        }

        return '';
    }

    /**
     * @param $qrCode
     * @param $pin
     * @return PointOfSale
     */
    public static function getPointOfSaleByQrCodeOrPin($qrCode, $pin = null)
    {
        if ($qrCode) {
            return self::where('qr_code', $qrCode)->first();
        } elseif ($pin) {
            return self::where('pin', $pin)->first();
        }

        return null;
    }

    public static function getPointOfSaleMerchantByQrCode($qrCode)
    {
        $pointOfSale = self::where('qr_code', $qrCode)->first();
        if ($pointOfSale) {
            return Merchant::find($pointOfSale->merchant_id);
        } else {
            return null;
        }
    }

    public static function getPointOfSaleCompanyByQrCode($qrCode)
    {
        $pointOfSale = self::where('qr_code', $qrCode)->first();
        if ($pointOfSale) {
            $merchant = Merchant::find($pointOfSale->merchant_id);

            return Company::find($merchant->company_id);
        } else {
            return null;
        }
    }

    public static function generatePin()
    {
        $number = mt_rand(10000000, 99999999);
        if (self::where('pin', $number)->exists()) {
            return self::generatePin();
        }

        return $number;
    }
}
