<?php

namespace Imawrsham\PayfixPackage\Models;

use Illuminate\Database\Eloquent\Model;
use Imawrsham\PayfixPackage\Models\Alarms;
use Imawrsham\PayfixPackage\Models\Device;
use Imawrsham\PayfixPackage\Models\Employee;
use Imawrsham\PayfixPackage\Models\Transaction;

class AlarmsLogs extends Model
{
    protected $fillable = [
        'alarm_id',
        'device_id',
        'employee_id',
        'transaction_id',
        'action_value'];

    public function alarm()
    {
        return $this->hasOne(Alarms::class, 'id', 'alarm_id');
    }

    public function device()
    {
        return $this->hasOne(Device::class, 'id', 'device_id');
    }

    public function employee()
    {
        return $this->hasOne(Employee::class, 'id', 'employee_id');
    }

    public function transaction()
    {
        return $this->hasOne(Transaction::class, 'id', 'transaction_id');
    }

}
