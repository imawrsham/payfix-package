<?php

namespace Imawrsham\PayfixPackage\Models;

use App\Scopes\Entities\ActiveCompanyScope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Company;
use Imawrsham\PayfixPackage\Models\QrCode;

class Customer extends Model
{
    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'company',
        'email',
        'iban',
        'bic',
        'bank_name',
        'street',
        'plz',
        'city',
        'country',
        'qr_code_id',
        'credit_card_number',
        'credit_card_holder',
        'credit_card_cvc',
        'credit_card_expiry',
        'company_id',
    ];

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    public static function boot()
    {
        parent::boot();
        static::addGlobalScope(new ActiveCompanyScope);
    }

    public function ageCategoryName()
    {
        return $this->ageCategory ? $this->ageCategory->name : '';
    }

    public function qrCodeName()
    {
        return $this->qrCode ? $this->qrCode->code : '';
    }

    public function qrCode()
    {
        return $this->hasOne(QrCode::class, 'id', 'qr_code_id');
    }

    public function company()
    {
        return $this->belongsTo(Company::class, 'company_id', 'id');
    }

    public function fullName()
    {
        return $this->first_name.' '.$this->last_name;
    }
}
