<?php

namespace Imawrsham\PayfixPackage\Models;

use App\Scopes\Entities\ActiveCompanyScope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Company;

class DepositAlarmConfiguration extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'company_id',
        'device_count_limit',
        'device_price_limit',
        'normal_order_count_limit',
        'normal_order_price_limit',
        'device_customer_price_limit',
        'random_count_limit',
        'notification_email_address',
    ];

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    public static function boot()
    {
        parent::boot();
        static::addGlobalScope(new ActiveCompanyScope);
    }

    public function company()
    {
        return $this->belongsTo(Company::class, 'company_id', 'id');
    }
}
