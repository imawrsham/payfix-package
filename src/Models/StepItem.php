<?php

namespace Imawrsham\PayfixPackage\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Imawrsham\PayfixPackage\Models\Article;
use Imawrsham\PayfixPackage\Models\MainModel;
use Imawrsham\PayfixPackage\Models\Step;

class StepItem extends MainModel
{
    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'step_id',
        'article_id',
        'priority',
    ];

    public function article()
    {
        return $this->belongsTo(Article::class, 'article_id', 'id');
    }

    public function step()
    {
        return $this->belongsTo(Step::class, 'step_id', 'id');
    }
}
