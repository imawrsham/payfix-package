<?php

namespace Imawrsham\PayfixPackage\Models;

use Illuminate\Database\Eloquent\Model;
use Imawrsham\PayfixPackage\Models\DailyClosing;

class DailyClosingItem extends Model
{
    public function DailyClosing()
    {
        return $this->belongsTo(DailyClosing::class, 'daily_closing_id', 'id');
    }
}
