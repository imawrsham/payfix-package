<?php

namespace Imawrsham\PayfixPackage\Models;

use Illuminate\Database\Eloquent\Model;

class QrCodeEventBalance extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'qr_code_id',
        'event_id',
        'balance',
    ];
}
