<?php

namespace Imawrsham\PayfixPackage\Models;

use Illuminate\Database\Eloquent\Model;
use Imawrsham\PayfixPackage\Models\Article;
use Imawrsham\PayfixPackage\Models\Printer;

class ArticlePrinter extends Model
{
//    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'article_id',
        'printer_id',
        'name',
    ];

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    public static function boot()
    {
        parent::boot();
    }

    public function article()
    {
        return $this->belongsTo(Article::class);
    }

    public function printer()
    {
        return $this->belongsTo(Printer::class);
    }
}
