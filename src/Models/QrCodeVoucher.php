<?php

namespace Imawrsham\PayfixPackage\Models;

use Illuminate\Database\Eloquent\Model;
use Imawrsham\PayfixPackage\Models\Article;
use Imawrsham\PayfixPackage\Models\QrCode;
use Imawrsham\PayfixPackage\Models\Transaction;
use Imawrsham\PayfixPackage\Models\Voucher;

class QrCodeVoucher extends Model
{
    public function qrCode()
    {
        return $this->hasOne(QrCode::class, 'id', 'qr_code_id');
    }

    public function transaction()
    {
        return $this->hasOne(Transaction::class, 'id', 'transaction_id');
    }

    public function article()
    {
        return $this->hasOne(Article::class, 'id', 'article_id');
    }

    public function voucher()
    {
        return $this->hasOne(Voucher::class, 'id', 'voucher_id');
    }
}
