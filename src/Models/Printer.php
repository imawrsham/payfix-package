<?php

namespace Imawrsham\PayfixPackage\Models;

use App\Scopes\Entities\PrinterScope;
use Illuminate\Database\Eloquent\SoftDeletes;
use Imawrsham\PayfixPackage\Models\MainModel;
use Imawrsham\PayfixPackage\Models\Merchant;
use Imawrsham\PayfixPackage\Models\PointOfSale;
use Imawrsham\PayfixPackage\Models\PrinterAllotment;

class Printer extends MainModel
{
    use SoftDeletes;

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    public static function boot()
    {
        parent::boot();
        static::addGlobalScope(new PrinterScope);
    }

    public function merchant()
    {
        return $this->belongsTo(Merchant::class, 'merchant_id', 'id');
    }

    public function pointOfSales()
    {
        return $this->belongsToMany(PointOfSale::class, 'printer_allotments', 'printer_id', 'point_of_sale_id');
    }

    public function printerAllotments()
    {
        return $this->hasMany(PrinterAllotment::class, 'printer_id', 'id');
    }

    public function pointOfSaleIds()
    {
        return $this->printerAllotments->pluck('point_of_sale_id')->toArray();
    }
}
