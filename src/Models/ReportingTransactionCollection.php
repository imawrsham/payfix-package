<?php

namespace Imawrsham\PayfixPackage\Models;

use Illuminate\Database\Eloquent\Collection;
use Imawrsham\PayfixPackage\Models\Transaction;

class ReportingTransactionCollection extends Collection
{
    public static function prepareTransactions(Collection $transactions)
    {
        foreach ($transactions as $transaction) {
            /** @var Transaction $transaction */

            $transaction['point_of_sale'] = $transaction->pointOfSaleWithTrashed;

            if ($transaction->pointOfSaleWithTrashed) {
                $transaction['point_of_sale']['merchant'] = $transaction->pointOfSaleWithTrashed->merchant;
            }

            $transaction['company'] = $transaction->company;
            $transaction['event'] = $transaction->event;
            $transaction['employee'] = $transaction->employee;
            $transaction['account'] = $transaction->account;
            $transaction['device'] = $transaction->device;

            $transaction['transactionDetails'] = $transaction->transactionDetailsWithArticle;
            $transaction['cardPaymentDetail'] = $transaction->cardPaymentDetail;
            $transaction['voucher'] = $transaction->voucher;
            $transaction['main_transaction'] = $transaction['pay_type'] != Transaction::TRANSACTION_PARTIAL_TYPE_IS_PARTIAL;

            if ($transaction->occasion_id) {
                $transaction['occasion'] = $transaction->occasion;
            }

            if ($transaction->qr_code_id) {
                $transaction['qr_code'] = $transaction->qrCode;
                $transaction['qr_code']['with_invoice'] = $transaction->qrCode->balance >= 0 ? 0 : 1;
            }

        }

        return $transactions;
    }
}
