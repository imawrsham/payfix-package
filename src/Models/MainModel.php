<?php

namespace Imawrsham\PayfixPackage\Models;

use App\Events\ModelChange;
use Fico7489\Laravel\Pivot\Traits\PivotEventTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class MainModel extends Model
{
    use PivotEventTrait;

    protected $dispatchesEvents = [
        'created' => ModelChange::class,
        'updated'=> ModelChange::class,
        'deleted'=> ModelChange::class,
//        'restored'=> ModelChange::class,
    ];

    public static function boot()
    {
        parent::boot();

        static::pivotAttached(function ($model, $relationName, $pivotIds, $pivotIdsAttributes) {
            self::callModelChangeEvent('insert', $model, $relationName, $pivotIds);
        });

        static::pivotDetached(function ($model, $relationName, $pivotIds) {
            self::callModelChangeEvent('delete', $model, $relationName, $pivotIds);
        });
    }

    public static function callModelChangeEvent($actionName, $model, $relationName, $pivotIds)
    {
        $pointOfSaleId = self::getEventPointOfSaleId($model);
        $modelName = 'App\\'.Str::studly(Str::singular($relationName));

        foreach ($pivotIds as $pivotId) {
            if (class_exists($modelName)) {
                $PivotModel = (new $modelName)::where('id', $pivotId)->first();
                event(new ModelChange($PivotModel, $actionName, $pointOfSaleId));
            }
        }
    }

    public static function getEventPointOfSaleId($model)
    {
        $pointOfSaleId = null;
        $tableName = $model->getTable();
        switch ($tableName) {
            case 'point_of_sales':
                $pointOfSaleId = $model->id;
                break;
        }

        return $pointOfSaleId;
    }
}
