<?php

namespace Imawrsham\PayfixPackage\Models;

use App\Scopes\Entities\LocationScope;
use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    protected $fillable = [
        'name',
    ];

    public static function boot()
    {
        parent::boot();
        static::addGlobalScope(new LocationScope);
    }
}
