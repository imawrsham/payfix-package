<?php

namespace Imawrsham\PayfixPackage\Models;

use App\Scopes\Entities\ActiveCompanyScope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Company;
use Imawrsham\PayfixPackage\Models\Credit;
use App\Event;
use Imawrsham\PayfixPackage\Models\Voucher;

class QrCodeCategory extends Model
{
    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'company_id',
        'name',
        'negative_booking',
        'can_book_deposit',
    ];

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    public static function boot()
    {
        parent::boot();
        static::addGlobalScope(new ActiveCompanyScope);
    }

    // relations

    public function company()
    {
        return $this->hasOne(Company::class, 'id', 'company_id');
    }

    public function negativeBookingAmount()
    {
        return number_format($this->negative_booking, 2, '.', ',');
    }

    public function events()
    {
        return $this->belongsToMany(Event::class, 'qr_code_category_events', 'qr_code_category_id', 'event_id');
    }

    public function eventNames()
    {
        return implode(', ', $this->events()->pluck('events.name')->toArray());
    }

    public function eventIds()
    {
        return $this->events()->pluck('events.id')->toArray();
    }

    public function vouchers()
    {
        return $this->belongsToMany(Voucher::class, 'qr_code_category_vouchers', 'qr_code_category_id', 'voucher_id')->withPivot('count', 'event_id');
    }

    public function getEventVouchers($eventId)
    {
        return $this->belongsToMany(Voucher::class, 'qr_code_category_vouchers', 'qr_code_category_id', 'voucher_id')->withPivot('count', 'event_id')->wherePivot('event_id', $eventId);
    }

    public function getAciveEventVouchers($eventId)
    {
        return $this->getEventVouchers($eventId)
            ->where('valid_to', '>=', date('Y-m-d'))
            ->where('valid_from', '<=', date('Y-m-d'))
            ->get();
    }

    public function credits()
    {
        return $this->belongsToMany(Voucher::class, 'qr_code_category_credits', 'qr_code_category_id', 'credit_id')->withPivot('count', 'event_id');
    }

    public function getEventCredits($eventId)
    {
        return $this->vouchers->where('qr_code_category_credits.event_id', $eventId);
    }

    public function voucherNames()
    {
        return implode(', ', $this->vouchers()->pluck('vouchers.name')->toArray());
    }

    /**
     * @param array $qrCodeCategoryVouchers
     * @param Voucher $voucher
     * @return bool
     */
    public function isAllotmentExists($qrCodeCategoryVouchers, Voucher $voucher)
    {
        if ($qrCodeCategoryVouchers) {
            foreach ($qrCodeCategoryVouchers as $allotment) {
                if ($allotment->qr_code_category_id === $this->id and
                    $allotment->voucher_id === $voucher->id
                ) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * @param $qrCodeCategoryVouchers
     * @param Voucher $voucher
     * @return string
     */
    public function getVoucherAllotmentExists($qrCodeCategoryVouchers, Voucher $voucher)
    {
        if ($qrCodeCategoryVouchers) {
            foreach ($qrCodeCategoryVouchers as $allotment) {
                if ($allotment->qr_code_category_id === $this->id and
                    $allotment->voucher_id === $voucher->id
                ) {
                    return $allotment;
                }
            }
        }

        return null;
    }

    /**
     * @param $qrCodeCategoryCredits
     * @param Credit $credit
     * @return string
     */
    public function getCreditAllotmentExists($qrCodeCategoryCredits, Credit $credit)
    {
        if ($qrCodeCategoryCredits) {
            foreach ($qrCodeCategoryCredits as $allotment) {
                if ($allotment->qr_code_category_id === $this->id and
                    $allotment->credit_id === $credit->id
                ) {
                    return $allotment;
                }
            }
        }

        return null;
    }
}
