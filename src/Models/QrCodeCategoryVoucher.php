<?php

namespace Imawrsham\PayfixPackage\Models;

use App\Scopes\Entities\QrCodeCategoryVoucherScope;
use Illuminate\Database\Eloquent\Model;
use App\Event;
use Imawrsham\PayfixPackage\Models\QrCodeCategory;
use Imawrsham\PayfixPackage\Models\Voucher;

class QrCodeCategoryVoucher extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'qr_code_category_id',
        'voucher_id',
        'count',
        'event_id',
    ];

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    public static function boot()
    {
        parent::boot();
        static::addGlobalScope(new QrCodeCategoryVoucherScope);
    }

    protected $primaryKey = ['qr_code_category_id', 'voucher_id'];
    public $incrementing = false;

    // relations

    public function qrCodeCategory()
    {
        return $this->hasOne(QrCodeCategory::class, 'id', 'qr_code_category_id');
    }

    public function voucher()
    {
        return $this->hasOne(Voucher::class, 'id', 'voucher_id');
    }

    public function event()
    {
        return $this->hasOne(Event::class, 'id', 'event_id');
    }

    /**
     * Set the keys for a save update query.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    protected function setKeysForSaveQuery($query)
    {
        $keys = $this->getKeyName();
        if (! is_array($keys)) {
            return parent::setKeysForSaveQuery($query);
        }

        foreach ($keys as $keyName) {
            $query->where($keyName, '=', $this->getKeyForSaveQuery($keyName));
        }

        return $query;
    }

    /**
     * Get the primary key value for a save query.
     *
     * @param mixed $keyName
     * @return mixed
     */
    protected function getKeyForSaveQuery($keyName = null)
    {
        if (is_null($keyName)) {
            $keyName = $this->getKeyName();
        }

        if (isset($this->original[$keyName])) {
            return $this->original[$keyName];
        }
    }
}
