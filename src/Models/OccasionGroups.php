<?php

namespace Imawrsham\PayfixPackage\Models;

use App\Scopes\Entities\ActiveCompanyScope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Imawrsham\PayfixPackage\Models\Occasions;
use Imawrsham\PayfixPackage\Models\PointOfSale;

class OccasionGroups extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
    ];

    public static function boot()
    {
        parent::boot();
        static::addGlobalScope(new ActiveCompanyScope);
    }

    public function occasions()
    {
        return $this->hasMany(Occasions::class, 'occasion_groups_id');
    }

    public function pointOfSale()
    {
        return $this->hasMany(PointOfSale::class, 'occasion_groups_id');
    }
}
