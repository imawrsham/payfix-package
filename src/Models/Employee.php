<?php

namespace Imawrsham\PayfixPackage\Models;

use App\Events\ModelChange;
use App\Scopes\Entities\ActiveCompanyScope;
use Illuminate\Database\Eloquent\SoftDeletes;
use Imawrsham\PayfixPackage\Models\MainModel;
use Imawrsham\PayfixPackage\Models\Merchant;
use Imawrsham\PayfixPackage\Models\PointOfSale;
use Imawrsham\PayfixPackage\Models\QrCode;

class Employee extends MainModel
{
    use SoftDeletes;

//    protected $dispatchesEvents = [
//        'updating'=> ModelChange::class,
//    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'personnel_number',
        'sell',
        'cancel',
        'age_verification',
        'bar_manager',
        'deposit_collection',
        'charge',
        'payout',
        'daily_close',
        'point_of_sale_id',
        'qr_code_id',
        'language',
        'pin',
        'point_of_sale_overal_access',
        'merchant_overal_access',
        'tip_enable',
        'tip_last_name',
        'tip_first_name',
        'tip_address',
        'tip_post_code',
        'tip_place',
        'tip_iban',
        'tip_bic',
        'tip_bank_name'
    ];

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    public static function boot()
    {
        parent::boot();
        static::addGlobalScope(new ActiveCompanyScope);
    }

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->dispatchesEvents['updating'] = ModelChange::class;
    }

    public function fullLanguage()
    {
        switch ($this->language) {
            case 'German':
                return 'Deutsch';
            case 'French':
                return 'Französisch';
            case 'Italian':
                return 'Italienisch';
            case 'English':
                return 'English';
        }

        return '';
    }

    public function fullName()
    {
        return $this->personnel_number.' - '.$this->first_name.' '.$this->last_name;
    }

    public function merchantName()
    {
        return $this->merchant ? $this->merchant->name : '';
    }

    public function merchant()
    {
        return $this->pointOfSale->hasOne(Merchant::class, 'id', 'merchant_id');
    }

    public function pointOfSaleName()
    {
        return $this->pointOfSale ? $this->pointOfSale->name : '';
    }

    public function pointOfSale()
    {
        return $this->hasOne(PointOfSale::class, 'id', 'point_of_sale_id');
    }

    public function qrCodeName()
    {
        return $this->qrCode ? $this->qrCode->code : '';
    }

    public function qrCode()
    {
        return $this->hasOne(QrCode::class, 'id', 'qr_code_id');
    }

    public static function getEmployeeById($employeeId)
    {
        return self::where('id', $employeeId)->first();
    }
}
