<?php

namespace Imawrsham\PayfixPackage\Models;

use App\Scopes\Entities\ActiveCompanyScope;
use Illuminate\Database\Eloquent\Model;

class AgeCategory extends Model
{
    protected $fillable = [
        'name',
    ];

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    public static function boot()
    {
        parent::boot();
        static::addGlobalScope(new ActiveCompanyScope);
    }

    public function canBeEdited()
    {
        return true;
    }
}
