<?php

namespace Imawrsham\PayfixPackage\Models;

use Illuminate\Database\Eloquent\Model;
use Imawrsham\PayfixPackage\Models\CardPaymentReceipt;

class CardPaymentDetail extends Model
{
    public function cardPaymentReceipts()
    {
        return $this->hasMany(CardPaymentReceipt::class, 'card_payment_detail_id', 'id');
    }
}
