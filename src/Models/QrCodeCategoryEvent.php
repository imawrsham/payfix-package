<?php

namespace Imawrsham\PayfixPackage\Models;

use Illuminate\Database\Eloquent\Model;
use App\Event;
use Imawrsham\PayfixPackage\Models\QrCodeCategory;

class QrCodeCategoryEvent extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'qr_code_category_id',
        'event_id',
    ];

    // relations

    public function qrCodeCategory()
    {
        return $this->hasOne(QrCodeCategory::class, 'id', 'qr_code_category_id');
    }

    public function event()
    {
        return $this->hasOne(Event::class, 'id', 'event_id');
    }
}
