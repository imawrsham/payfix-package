<?php

namespace Imawrsham\PayfixPackage\Models;

use App\Scopes\Entities\EventCreditScope;
use Illuminate\Database\Eloquent\Model;
use App\Company;
use Imawrsham\PayfixPackage\Models\Credit;
use App\Event;

class EventCredit extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'company_id',
        'event_id',
        'credit_id',
    ];

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    public static function boot()
    {
        parent::boot();
        static::addGlobalScope(new EventCreditScope);
    }

    public function companyName()
    {
        return $this->company->name;
    }

    public function company()
    {
        return $this->hasOne(Company::class, 'id', 'company_id');
    }

    public function eventName()
    {
        return $this->event->name;
    }

    public function event()
    {
        return $this->hasOne(Event::class, 'id', 'event_id');
    }

    public function creditName()
    {
        return $this->credit->name;
    }

    public function credit()
    {
        return $this->hasOne(Credit::class, 'id', 'credit_id');
    }
}
