<?php

namespace Imawrsham\PayfixPackage\Models;

use App\Scopes\Entities\ActiveCompanyScope;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Imawrsham\PayfixPackage\Models\PointOfSale;
use Laravel\Passport\HasApiTokens;

class Device extends Authenticatable
{
    use HasApiTokens;
    use SoftDeletes;

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    public static function boot()
    {
        parent::boot();
        static::addGlobalScope(new ActiveCompanyScope);
    }

    public function pointOfSale()
    {
        return $this->hasOne(PointOfSale::class, 'id', 'point_of_sale_id');
    }

    public function pointOfSaleWithTrashed()
    {
        return $this->hasOne(PointOfSale::class, 'id', 'point_of_sale_id')->withTrashed();
    }

    public static function getDeviceByDeviceId($deviceId)
    {
        return self::where('device_id', $deviceId)
            ->where('unregistered', false)->first();
    }

    public static function getDeviceByDeviceIdAndPointOfSaleId($deviceId, $pointOfSaleId)
    {
        return self::where('device_id', $deviceId)
            ->where('point_of_sale_id', $pointOfSaleId)
            ->where('unregistered', false)->first();
    }

    public static function getDevicesByDeviceId($deviceId)
    {
        return self::where('device_id', $deviceId)
            ->where('unregistered', false)->get();
    }

    public static function getPointOfSaleId($deviceId, $pointOfSaleId)
    {
        if ($pointOfSaleId) {
            return $pointOfSaleId;
        } else {
            return self::getDeviceByDeviceId($deviceId)->point_of_sale_id;
        }
    }
}
