<?php

namespace Imawrsham\PayfixPackage\Models;

use Illuminate\Database\Eloquent\Collection;
use Imawrsham\PayfixPackage\Models\Merchant;

class ReportingOccasionsWithCompanyId extends Collection
{
    public static function prepareOccasions(Collection $occasions)
    {
        foreach ($occasions as $occasion) {
            /** @var Merchant $merchant */
            $occasion['company_id'] = $occasion->occasionGroups->company_id;
        }

        return $occasions;
    }
}
