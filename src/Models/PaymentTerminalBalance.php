<?php

namespace Imawrsham\PayfixPackage\Models;

use Illuminate\Database\Eloquent\Model;

class PaymentTerminalBalance extends Model
{
    //
    public function getPrintData()
    {
        return json_decode($this->value)->printData->receipts;
    }
}
