<?php

namespace Imawrsham\PayfixPackage\Models;

use Illuminate\Database\Eloquent\Model;
use Imawrsham\PayfixPackage\Models\Credit;
use Imawrsham\PayfixPackage\Models\QrCode;
use Imawrsham\PayfixPackage\Models\Transaction;

class QrCodeCredit extends Model
{
    public function qrCode()
    {
        return $this->hasOne(QrCode::class, 'id', 'qr_code_id');
    }

    public function transaction()
    {
        return $this->hasOne(Transaction::class, 'id', 'transaction_id');
    }

    public function credit()
    {
        return $this->hasOne(Credit::class, 'id', 'credit_id');
    }
}
