<?php

namespace Imawrsham\PayfixPackage\Models;

use App\Scopes\Entities\ActiveCompanyScope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Company;
use Imawrsham\PayfixPackage\Models\EventCredit;

class Credit extends Model
{
    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'company_id',
        'name',
        'amount',
        'show_in_e_order',
    ];

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    public static function boot()
    {
        parent::boot();
        static::addGlobalScope(new ActiveCompanyScope);
    }

    public function companyName()
    {
        return $this->company->name;
    }

    public function company()
    {
        return $this->hasOne(Company::class, 'id', 'company_id');
    }

    public function eventCredits()
    {
        return $this->hasMany(EventCredit::class, 'credit_id', 'id')->get();
    }

    public function eventNames()
    {
        return implode(', ', $this->eventCredits()->pluck('event.name')->all());
    }

    public function displayAmount()
    {
        return number_format($this->amount, 2, '.', ',');
    }
}
