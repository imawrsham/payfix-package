<?php

namespace Imawrsham\PayfixPackage\Models;

use App\Scopes\Entities\ArticleScope;
use Illuminate\Database\Eloquent\SoftDeletes;
use Imawrsham\PayfixPackage\Models\AgeCategory;
use Imawrsham\PayfixPackage\Models\ArticleAllotment;
use Imawrsham\PayfixPackage\Models\ArticleGroup;
use Imawrsham\PayfixPackage\Models\ArticlePackage;
use Imawrsham\PayfixPackage\Models\Assortment;
use App\Image;
use Imawrsham\PayfixPackage\Models\MainModel;
use Imawrsham\PayfixPackage\Models\Mwst;
use Imawrsham\PayfixPackage\Models\Printer;
use Imawrsham\PayfixPackage\Models\Voucher;
use Milon\Barcode\DNS1D;

class Article extends MainModel
{
    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'article_group_id',
        'article_package_id',
        'article_sub_group_id',
        'name',
        'price',
        'mwst_id',
        'mwst_takeaway_id',
        'deposit_id',
        'age_rating',
        'voucher_id',
        'color',
        'sorting',
        'favourite',
        'free',
        'qr_code_forbidden',
        'deposit',
        'ean',
        'image_id',
        'is_sold',
        'name_in_checkout',
        'discountable',
        'double_receipt',
        'has_integrated_printer',
        'is_ordermood',
        'hidden_article',
        'hidden_number',
        'show_article_small',
        'activate_arrow',
        'tip',
        'article_description',
        'external_article_id',
    ];

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    public static function boot()
    {
        parent::boot();
        static::addGlobalScope(new ArticleScope);
        //static::addGlobalScope(new ReplacedScope);
    }

    // functions

    public function articleGroupName()
    {
        return $this->articleGroup ? $this->articleGroup->name : '';
    }

    public function articleSubGroupName()
    {
        return $this->articleSubGroup ? $this->articleSubGroup->name : '';
    }

    public function mwstAmount()
    {
        return $this->mwst ? $this->mwst->mwst.'%' : '';
    }

    public function mwstTakeawayAmount()
    {
        return $this->mwstTakeaway ? $this->mwstTakeaway->mwst.'%' : '';
    }

    public function type()
    {
        return 'normal';
    }

    // relations

    public function articleGroup()
    {
        return $this->hasOne(ArticleGroup::class, 'id', 'article_group_id');
    }


    public function reportArticleGroup()
    {
        return $this->articleGroup()->select('name');
    }

    public function mwstTakeaway()
    {
        return $this->belongsTo(Mwst::class, 'mwst_takeaway_id', 'id');
    }

    public function articleSubGroup()
    {
        return $this->hasOne(ArticleGroup::class, 'id', 'article_sub_group_id');
    }

    public function mwst()
    {
        return $this->hasOne(Mwst::class, 'id', 'mwst_id');
    }

    public function depositName()
    {
        return $this->depositArticle ? $this->depositArticle->name : '-';
    }

    public function depositArticle()
    {
        return $this->hasOne(self::class, 'id', 'deposit_id');
    }

    public function voucherName()
    {
        return $this->voucher ? $this->voucher->name : '-';
    }

    public function voucher()
    {
        return $this->hasOne(Voucher::class, 'id', 'voucher_id');
    }

    public function ageCategoryName()
    {
        return $this->ageCategory ? $this->ageCategory->name : '-';
    }

    public function ageCategory()
    {
        return $this->hasOne(AgeCategory::class, 'id', 'age_category_id');
    }

    public function priceAmount()
    {
        return number_format($this->price, 2, '.', ',');
    }

    public function hasArticleAllotment()
    {
        return $this->articleAllotments()->count() > 0;
    }

    public function articleAllotments()
    {
        return $this->hasMany(ArticleAllotment::class, 'article_id', 'id');
    }

    public function pointOfSaleIds()
    {
        return $this->articleAllotments->pluck('point_of_sale_id')->toArray();
    }

    public function assortments()
    {
        return $this->belongsToMany(Assortment::class, 'article_assortments', 'article_id', 'assortment_id');
    }

    public function assortmentIds()
    {
        return $this->assortments()->pluck('assortments.id')->toArray();
    }

    public function image()
    {
        return $this->hasOne(Image::class, 'id', 'image_id');
    }

    public function printers()
    {
        return $this->belongsToMany(Printer::class, 'article_printers', 'article_id', 'printer_id');
    }

    public function printerIds()
    {
        return $this->printers()->pluck('printers.id')->toArray();
    }

    public function articlePackage()
    {
        return $this->belongsTo(ArticlePackage::class, 'article_package_id', 'id');
    }

    public function isEanValid()
    {
        try {
            $dNS1D = new DNS1D();
            if (strlen($this->ean) == 13) {
                $dNS1D->__call('getBarcodeHTML', [$this->ean, 'EAN13', 1, 40, 'black', false]);

                return true;
            } elseif (strlen($this->ean) == 8) {
                $dNS1D->__call('getBarcodeHTML', [$this->ean, 'EAN8', 1, 40, 'black', false]);

                return true;
            } else {
                return false;
            }
        } catch (\Milon\Barcode\WrongCheckDigitException $exception) {
            return false;
        }
    }

    public function hasImage()
    {
        return ! is_null($this->image_id);
    }

    public function mwst1()
    {
        return $this->belongsToMany(Mwst::class, 'mwst_companies', 'mwst_id', 'company_id');
    }

    public function mwst1Ids()
    {
        return $this->mwst1->pluck('id')->toArray();
    }

}
