<?php

namespace Imawrsham\PayfixPackage\Models;

use App\Scopes\Entities\RestaurantTableScope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Imawrsham\PayfixPackage\Models\OpenRestaurantTable;
use Imawrsham\PayfixPackage\Models\OpenRestaurantTableBasket;
use Imawrsham\PayfixPackage\Models\RestaurantTableGroup;

class RestaurantTable extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'restaurant_table_group_id',
        'number',
    ];

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    public static function boot()
    {
        parent::boot();
        static::addGlobalScope(new RestaurantTableScope);
    }

    public function restaurantTableGroup()
    {
        return $this->hasOne(RestaurantTableGroup::class, 'id', 'restaurant_table_group_id');
    }

    public function openRestaurantTables()
    {
        return $this->hasMany(OpenRestaurantTable::class, 'restaurant_table_id', 'id');
    }

    public function openRestaurantTableBaskets()
    {
        return $this->hasMany(OpenRestaurantTableBasket::class, 'restaurant_table_id', 'id');
    }
}
