<?php

namespace Imawrsham\PayfixPackage\Models;

use Illuminate\Database\Eloquent\Model;
use Imawrsham\PayfixPackage\Models\Merchant;
use Imawrsham\PayfixPackage\Models\PointOfSale;
use Imawrsham\PayfixPackage\Models\Printer;

class PrinterAllotment extends Model
{
    public function merchant()
    {
        return $this->belongsTo(Merchant::class, 'merchant_id', 'id');
    }

    public function pointOfSale()
    {
        return $this->belongsTo(PointOfSale::class, 'point_of_sale_id', 'id');
    }

    public function printer()
    {
        return $this->belongsTo(Printer::class, 'printer_id', 'id');
    }
}
