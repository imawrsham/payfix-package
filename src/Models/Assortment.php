<?php

namespace Imawrsham\PayfixPackage\Models;

use App\Scopes\Entities\AssortmentScope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Imawrsham\PayfixPackage\Models\Merchant;

class Assortment extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'name',
        'is_active',
        'merchant_id',
    ];

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    public static function boot()
    {
        parent::boot();
        static::addGlobalScope(new AssortmentScope);
    }

    public function merchant()
    {
        return $this->belongsTo(Merchant::class, 'merchant_id', 'id');
    }

    public static function deactiveAll()
    {
        return self::where('is_active', true)->update(['is_active' => false]);
    }

    public static function deactivateAllMerchantAssortment($merchantId)
    {
        return self::where('is_active', true)
            ->where('merchant_id', $merchantId)
            ->update(['is_active' => false]);
    }
}
