<?php

namespace Imawrsham\PayfixPackage\Models;

use App\Events\ModelChange;
use App\Scopes\Entities\ArticlePackageScope;
use Illuminate\Database\Eloquent\SoftDeletes;
use Imawrsham\PayfixPackage\Models\Article;
use Imawrsham\PayfixPackage\Models\MainModel;
use Imawrsham\PayfixPackage\Models\Merchant;
use Imawrsham\PayfixPackage\Models\Step;

class ArticlePackage extends MainModel
{
    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'article_id',
        'name',
    ];

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    public static function boot()
    {
        parent::boot();
        static::addGlobalScope(new ArticlePackageScope);
    }

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->dispatchesEvents['updating'] = ModelChange::class;
    }

    public function steps()
    {
        return $this->hasMany(Step::class, 'article_package_id', 'id')->orderBy('priority', 'ASC');
    }

    public function articles()
    {
        return $this->hasMany(Article::class, 'article_package_id', 'id');
    }

    public function merchant()
    {
        return $this->belongsTo(Merchant::class);
    }
}
