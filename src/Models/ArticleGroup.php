<?php

namespace Imawrsham\PayfixPackage\Models;

use App\Scopes\Entities\ArticleGroupScope;
use Illuminate\Database\Eloquent\SoftDeletes;
use Imawrsham\PayfixPackage\Models\Article;
use App\Image;
use Imawrsham\PayfixPackage\Models\MainModel;
use Imawrsham\PayfixPackage\Models\Merchant;

class ArticleGroup extends MainModel
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'merchant_id',
        'main_article_group_id',
        'is_sub_group',
        'name',
        'global',
        'color',
        'sorting',
    ];

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    public static function boot()
    {
        parent::boot();
        static::addGlobalScope(new ArticleGroupScope);
    }

    // functions

    public function canBeDeleted()
    {
        return boolval(count($this->articles) === 0) && boolval(count($this->subArticleGroups) === 0);
    }

    // relations

    public function articles()
    {
        return $this->hasMany(Article::class, 'article_group_id', 'id');
    }

    public function merchant()
    {
        return $this->hasOne(Merchant::class, 'id', 'merchant_id');
    }

    public function mainArticleGroup()
    {
        return $this->belongsTo(self::class, 'main_article_group_id', 'id');
    }

    public function subArticleGroups()
    {
        return $this->HasMany(self::class, 'main_article_group_id', 'id');
    }

    public function image()
    {
        return $this->hasOne(Image::class, 'id', 'image_id');
    }

    public function hasImage()
    {
        return ! is_null($this->image_id);
    }
}
