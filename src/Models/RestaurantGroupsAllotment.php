<?php

namespace Imawrsham\PayfixPackage\Models;

use Illuminate\Database\Eloquent\Model;
use Imawrsham\PayfixPackage\Models\PointOfSale;
use Imawrsham\PayfixPackage\Models\RestaurantTableGroup;

class RestaurantGroupsAllotment extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'restaurant_table_groups_id',
        'point_of_sale_id',
    ];

    // relations

    public function restaurantTableGroup()
    {
        return $this->hasOne(RestaurantTableGroup::class, 'id', 'restaurant_table_groups_id');
    }

    public function pointOfSale()
    {
        return $this->hasOne(PointOfSale::class, 'id', 'point_of_sale_id');
    }
}
