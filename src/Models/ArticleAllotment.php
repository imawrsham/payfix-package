<?php

namespace Imawrsham\PayfixPackage\Models;

use Imawrsham\PayfixPackage\Models\Article;
use Imawrsham\PayfixPackage\Models\MainModel;
use Imawrsham\PayfixPackage\Models\Merchant;
use Imawrsham\PayfixPackage\Models\PointOfSale;

class ArticleAllotment extends MainModel
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'merchant_id',
        'article_id',
        'point_of_sale_id',
    ];

    public function merchant()
    {
        return $this->hasOne(Merchant::class, 'id', 'merchant_id');
    }

    public function article()
    {
        return $this->hasOne(Article::class, 'id', 'article_id');
    }

    public function pointOfSale()
    {
        return $this->hasOne(PointOfSale::class, 'id', 'point_of_sale_id');
    }

    public static function getFavouriteByArticleIdAndPointOfSaleId($articleId, $pointOfSaleId)
    {
        return self::where('article_id', $articleId)
            ->where('point_of_sale_id', $pointOfSaleId)->first();
    }
}
