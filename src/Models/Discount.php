<?php

namespace Imawrsham\PayfixPackage\Models;

use App\Scopes\Entities\ActiveCompanyScope;
use Illuminate\Database\Eloquent\SoftDeletes;
use Imawrsham\PayfixPackage\Models\MainModel;

class Discount extends MainModel
{
    use SoftDeletes;

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    public static function boot()
    {
        parent::boot();
        static::addGlobalScope(new ActiveCompanyScope);
    }

    protected $fillable = [
        'name',
        'discount_amount',
        'is_active',
        'is_article_discount',
        'is_basket_discount',
        'company_id',
    ];
}
