<?php

namespace Imawrsham\PayfixPackage\Models;

use App\Scopes\Entities\ActiveCompanyScope;
use Illuminate\Database\Eloquent\Model;
use App\Company;
use Imawrsham\PayfixPackage\Models\DailyClosingItem;
use Imawrsham\PayfixPackage\Models\DailyClosingTip;
use Imawrsham\PayfixPackage\Models\Device;
use Imawrsham\PayfixPackage\Models\Employee;
use App\Event;
use Imawrsham\PayfixPackage\Models\PaymentTerminalBalance;
use Imawrsham\PayfixPackage\Models\PointOfSale;

class DailyClosing extends Model
{
    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    public static function boot()
    {
        parent::boot();
        static::addGlobalScope(new ActiveCompanyScope);
    }

    public function company()
    {
        return $this->hasOne(Company::class, 'id', 'company_id');
    }

    public function companyWithTrashed()
    {
        return $this->hasOne(Company::class, 'id', 'company_id')->withTrashed();
    }

    public function device()
    {
        return $this->hasOne(Device::class, 'id', 'device_id');
    }

    public function deviceWithTrashed()
    {
        return $this->hasOne(Device::class, 'id', 'device_id')->withTrashed();
    }

    public function event()
    {
        return $this->hasOne(Event::class, 'id', 'event_id');
    }

    public function eventWithTrashed()
    {
        return $this->hasOne(Event::class, 'id', 'event_id')->withTrashed();
    }

    public function employee()
    {
        return $this->hasOne(Employee::class, 'id', 'employee_id');
    }

    public function pointOfSale()
    {
        return $this->hasOne(PointOfSale::class, 'id', 'point_of_sale_id');
    }

    public function employeeWithTrashed()
    {
        return $this->hasOne(Employee::class, 'id', 'employee_id')->withTrashed();
    }

    public function PaymentTerminalBalances()
    {
        return $this->hasMany(PaymentTerminalBalance::class, 'daily_closing_id', 'id');
    }

    public function DailyClosingItems()
    {
        return $this->hasMany(DailyClosingItem::class, 'daily_closing_id', 'id');
    }

    public function DailyClosingTips()
    {
        return $this->hasMany(DailyClosingTip::class, 'daily_closing_id', 'id');
    }

    public function pointOfSaleWithTrashed()
    {
        return $this->hasOne(PointOfSale::class, 'id', 'point_of_sale_id')->withTrashed();
    }
}
