<?php

namespace Imawrsham\PayfixPackage\Models;

use App\Scopes\Entities\ActiveCompanyScope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Imawrsham\PayfixPackage\Models\Device;
use Imawrsham\PayfixPackage\Models\Employee;

class DepositAlarm extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'type',
        'time',
        'action',
        'limit',
        'response_time',
        'total_counter',
        'current_counter',
        'company_id',
        'device_id',
        'point_of_sale_id',
        'employee_id',
        'bar_manager_id',
        'qr_code_id',
        'card_number',
    ];

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    public static function boot()
    {
        parent::boot();
        static::addGlobalScope(new ActiveCompanyScope);
    }

    public function device()
    {
        return $this->belongsTo(Device::class, 'device_id', 'id');
    }

    public function employee()
    {
        return $this->belongsTo(Employee::class, 'employee_id', 'id');
    }

    public function barManager()
    {
        return $this->belongsTo(Employee::class, 'bar_manager_id', 'id');
    }
}
