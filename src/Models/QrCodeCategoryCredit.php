<?php

namespace Imawrsham\PayfixPackage\Models;

use App\Scopes\Entities\QrCodeCategoryCreditScope;
use Illuminate\Database\Eloquent\Model;
use Imawrsham\PayfixPackage\Models\Credit;
use App\Event;
use Imawrsham\PayfixPackage\Models\QrCodeCategory;

class QrCodeCategoryCredit extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'qr_code_category_id',
        'credit_id',
        'count',
        'event_id',
    ];

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    public static function boot()
    {
        parent::boot();
        static::addGlobalScope(new QrCodeCategoryCreditScope);
    }

    protected $primaryKey = ['qr_code_category_id', 'credit_id'];
    public $incrementing = false;

    // relations

    public function qrCodeCategory()
    {
        return $this->hasOne(QrCodeCategory::class, 'id', 'qr_code_category_id');
    }

    public function credit()
    {
        return $this->hasOne(Credit::class, 'id', 'credit_id');
    }

    public function event()
    {
        return $this->hasOne(Event::class, 'id', 'event_id');
    }

    public function amount()
    {
        return $this->count * $this->credit->amount;
    }

    public function displayAmount()
    {
        return number_format($this->amount(), 2, '.', ',');
    }

    public function displayValue()
    {
        return $this->displayAmount().' ('.$this->count.'x)';
    }

    /**
     * Set the keys for a save update query.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    protected function setKeysForSaveQuery($query)
    {
        $keys = $this->getKeyName();
        if (! is_array($keys)) {
            return parent::setKeysForSaveQuery($query);
        }

        foreach ($keys as $keyName) {
            $query->where($keyName, '=', $this->getKeyForSaveQuery($keyName));
        }

        return $query;
    }

    /**
     * Get the primary key value for a save query.
     *
     * @param mixed $keyName
     * @return mixed
     */
    protected function getKeyForSaveQuery($keyName = null)
    {
        if (is_null($keyName)) {
            $keyName = $this->getKeyName();
        }

        if (isset($this->original[$keyName])) {
            return $this->original[$keyName];
        }
    }
}
