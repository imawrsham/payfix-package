<?php

namespace Imawrsham\PayfixPackage\Models;

use App\Scopes\Entities\QrCodeScope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Imawrsham\PayfixPackage\Models\AgeCategory;
use Imawrsham\PayfixPackage\Models\Credit;
use Imawrsham\PayfixPackage\Models\Customer;
use Imawrsham\PayfixPackage\Models\Employee;
use Imawrsham\PayfixPackage\Models\QrCodeCategory;
use Imawrsham\PayfixPackage\Models\Transaction;
use Imawrsham\PayfixPackage\Models\Voucher;

class QrCode extends Model
{
    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'qr_code_category_id',
        'age_category_id',
        'code',
        'pin',
        'info',
        'locked',
        'balance'
    ];

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    public static function boot()
    {
        parent::boot();
        static::addGlobalScope(new QrCodeScope);
    }

    // relations

    public function qrCodeCategory()
    {
        return $this->hasOne(QrCodeCategory::class, 'id', 'qr_code_category_id');
    }

    public function ageCategory()
    {
        return $this->belongsTo(AgeCategory::class, 'age_category_id', 'id');
    }

    public function qrCodeCategoryName()
    {
        return $this->qrCodeCategory->name;
    }

    public function employees()
    {
        return $this->hasMany(Employee::class, 'qr_code_id', 'id');
    }

    public function employeesWithTrashed()
    {
        return $this->hasMany(Employee::class, 'qr_code_id', 'id')->withTrashed();
    }

    public function customers()
    {
        return $this->hasMany(Customer::class, 'qr_code_id', 'id');
    }

    public function selectText()
    {
        return $this->code.' ('.$this->pin.')';
    }

    public static function getQrCodeByCode($qrCode)
    {
        return self::where('code', $qrCode)->first();
    }

    public function transactions()
    {
        return $this->hasMany(Transaction::class, 'qr_code_id', 'id');
    }

    public function usedVouchers()
    {
        return $this->belongsToMany(Voucher::class, 'qr_code_vouchers', 'qr_code_id', 'voucher_id');
    }

    public function usedCredits()
    {
        return $this->belongsToMany(Voucher::class, 'qr_code_credits', 'qr_code_id', 'credit_id');
    }

    public function isInEvent($eventId)
    {
        $events = $this->qrCodeCategory->events;
        if ($events) {
            foreach ($events as $event) {
                if ($event->id == $eventId){
                    return true;
                }
            }

        }

        return false;
    }
    public function addBalance($amount)
    {
        $this->balance += $amount;
        $this->save();
    }

    public function availableAmount()
    {
        $negative_booking = $this->qrCodeCategory->negative_booking;
        return -1 * ($negative_booking) + $this->balance;
    }

    public function getAvailableVouchers($eventId)
    {
        $result = [];
        $assignedVouchers = $this->qrCodeCategory->getAciveEventVouchers($eventId);
        $usedVouchers = $this->usedVouchers;
        foreach ($assignedVouchers as $assignedVoucher) {
            $usedCount = $usedVouchers->where('id', $assignedVoucher->id)->count();
            for ($i = 0; $i < $assignedVoucher->getRawOriginal('pivot_count') - $usedCount; $i++) {
                $result[] = Voucher::find($assignedVoucher->id);
            }
        }

        return $result;
    }

    public function getAvailableCredits($eventId)
    {
        $result = [];
        $assignedCredits = $this->qrCodeCategory->credits;
        $usedCredits = $this->usedCredits;
        foreach ($assignedCredits as $assignedCredit) {
            $usedCount = $usedCredits->where('id', $assignedCredit->id)->count();
            for ($i = 0; $i < $assignedCredit->getRawOriginal('pivot_count') - $usedCount; $i++) {
                $result[] = Credit::find($assignedCredit->id);
            }
        }

        return $result;
    }
}
