<?php

namespace Imawrsham\PayfixPackage;

use Illuminate\Support\Facades\Facade;

/**
 * @see \Imawrsham\PayfixPackage\PayfixModels
 */
class PayfixModelsFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'payfix-models';
    }
}
